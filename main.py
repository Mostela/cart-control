from fastapi.encoders import jsonable_encoder
from fastapi import FastAPI, Response, status
from model.create_cart import CreateCartAPI
from service.create_cart import CreateCartService
from service.delete_cart import DeleteCartService
from service.return_cart import ReturnCartService

app = FastAPI()


@app.post("/cart/")
async def root(item: CreateCartAPI, response: Response):
    json_compatible_item_data = jsonable_encoder(item)
    service = CreateCartService()
    response_svc = service.execute(CreateCartAPI(**json_compatible_item_data))
    response.status_code = status.HTTP_201_CREATED
    return response_svc.body


@app.get("/cart/{guid_user}")
async def get_cart(guid_user: str, response: Response):
    service = ReturnCartService()
    response_svc = service.execute({"guid_user": guid_user})
    response.status_code = response_svc.status
    return response_svc.body


@app.delete("/cart/{guid_user}/{guid_item}")
async def del_item_cart(guid_user: str, guid_item: str, response: Response):
    service = DeleteCartService()
    response_svc = service.execute({"guid_user": guid_user, "guid_item": guid_item})
    response.status_code = response_svc.status
    return response_svc.body

