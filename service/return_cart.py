from model.return_data import ReturnDataDTO, Return400DTO, Return200DTO
from repository.redis import RedisRepository
from service.abc_service import ServiceABS


class ReturnCartService(ServiceABS):
    def __init__(self):
        self.redis_repo = RedisRepository()

    def execute(self, data: dict) -> ReturnDataDTO:
        if data.get("guid_user"):
            return Return200DTO(body={"products": self.redis_repo.get_key(data.get("guid_user"))})
        return Return200DTO(body={"products": []})
