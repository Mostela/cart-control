import abc

from model.abc_model import ABCModel
from model.return_data import ReturnDataDTO


class ServiceABS(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def execute(self, data: ABCModel) -> ReturnDataDTO:
        pass
