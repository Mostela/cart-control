import datetime
import json
import os
import uuid

import requests

from model.order_saved import OrderSaved
from model.order_status import OrderStatus
from model.return_data import ReturnDataDTO, Return201DTO
from repository.rabbitmq import RabbitMqRepository
from repository.redis import RedisRepository
from service.abc_service import ServiceABS


class ReservePaymentService(ServiceABS):
    def __init__(self):
        self.redis_repo = RedisRepository()
        self.queue_repo = RabbitMqRepository(queue="payment")

    def execute(self, data: dict) -> ReturnDataDTO:
        errors_list = []
        order_saved = OrderSaved()

        order_saved.order_guid = uuid.uuid4().hex
        if data.get("items", []) and data.get("user_guid"):
            for item in data.get("items", []):

                # Check status stock product
                enable_sell = False
                result_cache = self.redis_repo.get_item(item)
                enable_sell = bool(result_cache) if bool(result_cache) else False
                if not enable_sell:
                    enable_sell = self.check_product_stock(item)
                if not enable_sell:
                    errors_list.append({"item_guid": item, "message": "item can't sold"})
                else:
                    order_saved.items.append(item)
                self.new_cache(item)
            order_saved.guid_user = data.get("user_guid")
            order_saved.date_created = str(datetime.datetime.today())
            order_saved.status = OrderStatus.RESERVED

            save_order_str = json.dumps(order_saved.to_json())
            self.queue_repo.send(save_order_str)
            self.redis_repo.save_item(order_saved.order_guid, save_order_str)
        return Return201DTO(body={"error": errors_list, "guid": order_saved.order_guid})

    def check_product_stock(self, guid_product: str) -> bool:
        try:
            req = requests.get(f"{os.getenv('STOCK_URL')}/product/sell/{guid_product}")
            if req.text == 'true':
                return True
            return False
        except Exception:
            return False

    def new_cache(self, key: str):
        self.redis_repo.save_item(key, str(self.check_product_stock(key)))
