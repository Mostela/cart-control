from model.cart import CartModel
from model.create_cart import CreateCartAPI
from model.return_data import ReturnDataDTO, Return400DTO, Return200DTO, Return201DTO
from repository.redis import RedisRepository
from service.abc_service import ServiceABS


class CreateCartService(ServiceABS):
    def __init__(self):
        self.redis_repo = RedisRepository()

    def execute(self, data: CreateCartAPI) -> ReturnDataDTO:
        if data.guid_user and data.guid_item:
            old_items = self.redis_repo.get_key(data.guid_user)
            if data.guid_item not in old_items:
                old_items.append(data.guid_item)
            to_save = CartModel(key=data.guid_user, array_values=old_items)
            self.redis_repo.save_key(to_save.guid_key, to_save.array_value)
            return Return201DTO(body={"guid": data.guid_user, "products": to_save.array_value})
        else:
            return Return400DTO(body="user or product not info")
