from model.abc_model import ABCModel
from model.return_data import ReturnDataDTO, Return400DTO, Return202DTO
from repository.redis import RedisRepository
from service.abc_service import ServiceABS


class DeleteCartService(ServiceABS):
    def __init__(self):
        self.redis_repo = RedisRepository()

    def execute(self, data: dict) -> ReturnDataDTO:
        if data.get("guid_user") and data.get("guid_item"):
            guid_user = data.get("guid_user")
            item_list = self.redis_repo.get_key(guid_user)
            if data.get("guid_item") in item_list:
                item_list.remove(data.get("guid_item"))
            self.redis_repo.save_key(guid_user, item_list)
            return Return202DTO(body={"products": item_list})
        return Return400DTO(body="Not found user or item")


