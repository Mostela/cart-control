import json

import redis


class RedisRepository:
    def __init__(self):
        self.connector = redis.Redis(host='localhost', port=6379, db=0)

    def save_key(self, key: str, value: list) -> bool:
        list_raw = ""
        for item in value:
            list_raw += f"{item};"
        return self.connector.set(key, list_raw)

    def save_item(self, key: str, value: str):
        return self.connector.set(key, value)

    def get_item(self, key: str) -> str:
        return self.connector.get(key)

    def get_key(self, key: str) -> list:
        data = self.connector.get(key)
        return_data = []

        if data is None:
            return []

        for item in data.decode().split(";"):
            if item != '':
                return_data.append(item)
        return return_data

    def clean_key(self, key: str):
        self.connector.delete(key)
