from kombu import Exchange, Queue, Connection


class RabbitMqRepository:
    def __init__(self, queue: str):
        self.connection = Connection('amqp://guest:guest@localhost:5672//')
        self.queue = queue

    def send(self, data: str):
        queue = self.connection.SimpleQueue(self.queue)
        queue.put(message=data)
        queue.close()
