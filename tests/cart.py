import unittest

from model.create_cart import CreateCartAPI
from service.create_cart import CreateCartService
from service.return_cart import ReturnCartService

extern = {
    "guid_user": "0012",
    "guid_item": "0001"
}


class CartTest(unittest.TestCase):
    _testMethodName = "Cart Multi Operation"

    def test_create_new(self):
        service = CreateCartService()
        data = CreateCartAPI(**extern)
        self.assertEqual(service.execute(data).status, 201)

    def test_return_products(self):
        service = ReturnCartService()
        response = service.execute(data=extern)
        self.assertIsNot(response.body.get("products"), [])
        self.assertGreaterEqual(len(response.body.get("products")), 1)


if __name__ == '__main__':
    unittest.main()
