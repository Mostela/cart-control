import unittest

from service.card_hash import CardHash
from service.reserved_payment import ReservePaymentService

example_payment_reserved = {
    "items": [
        "254b936f-e899-4309-9648-451661f102ac",
        "e6d9d486-4a58-49ea-9326-28b61cb1234c",
        "66e520f3-b891-4eea-999a-418affa4185b"
    ],
    "user_guid": "0007",
    "total_price": 600
}

example_payment_confirmed = {
    "payment_guid": "d576b30350424a19b773fa432baac85c",
    "method": {
        "type": "credit card",
        "price": "600",
        "installments": "1",
        "hash_card": "AAAAAAAAAEEE"
    }
}


class PaymentTestCase(unittest.TestCase):
    def test_reserved(self):
        service = ReservePaymentService()
        response = service.execute(example_payment_reserved)
        print(response.body)
        self.assertEqual(len(response.body.get("error")), 0)
        self.assertEqual(response.status, 201)
        self.assertIsNotNone(response.body.get("guid"))

    def test_create_key_payment(self):
        service = CardHash()
        data = service.create_key(example_payment_confirmed.get('payment_guid'))
        print(data)
        self.assertIsNotNone(data)


if __name__ == '__main__':
    unittest.main()
