from enum import Enum


class PaymentsType(Enum):
    CREDIT_CARD = 'credit card'
    DEBIT_CARD = 'debit card'
    MARKET_PAY = 'mercado pago'
    PAYPAY = 'paypal'
