import datetime

from model.order_status import OrderStatus


class OrderSaved:
    items: [str] = []
    guid_user: str
    order_guid: str
    date_created: str
    status: OrderStatus = None

    def to_json(self):
        return {
            "items": self.items,
            "guid_user": self.guid_user,
            "order_guid": self.order_guid,
            "date_created": self.date_created,
            "status": self.status.name
        }
