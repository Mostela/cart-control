from enum import Enum


class OrderStatus(Enum):
    RESERVED = 0
    PAYMENT = 1
    FINISH = 2
