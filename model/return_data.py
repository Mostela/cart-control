from fastapi import status


class ReturnDataDTO:
    body: dict
    status: status


class Return200DTO(ReturnDataDTO):
    def __init__(self, body: dict):
        self.body = body
        self.status = status.HTTP_200_OK


class Return201DTO(ReturnDataDTO):
    def __init__(self, body: dict):
        self.body = body
        self.status = status.HTTP_201_CREATED


class Return202DTO(ReturnDataDTO):
    def __init__(self, body: dict):
        self.body = body
        self.status = status.HTTP_202_ACCEPTED


class Return400DTO(ReturnDataDTO):
    def __init__(self, body: str):
        self.body = {
            "info": "Have problems to make this action",
            "problem": body
        }
        self.status = status.HTTP_400_BAD_REQUEST
