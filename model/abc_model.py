import abc


class ABCModel(metaclass=abc.ABCMeta):
    def to_class(self, fields: dict):
        return (setattr(self, field, fields.get(field)) for field in fields)
