from pydantic import BaseModel


class CreateCartAPI(BaseModel):
    guid_user: str
    guid_item: str
