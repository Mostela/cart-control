class CartModel:
    guid_key: str
    array_value: [str]

    def __init__(self, key: str, array_values: [str]):
        self.guid_key = key
        self.array_value = array_values

